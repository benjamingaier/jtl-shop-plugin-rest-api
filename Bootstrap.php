<?php declare(strict_types=1);
/**
 * @package Plugin\jtl_tmrapi
 * @author  Felix Moche
 */

namespace Plugin\jtl_tmrapi;

use JTL\Events\Dispatcher;
use JTL\Link\LinkInterface;
use JTL\Plugin\Bootstrapper;
use JTL\Router\Router;
use JTL\Shop;
use JTL\Shopsetting;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_tmrapi\Controllers\APIController;
use Plugin\jtl_tmrapi\Controllers\BackendController;
use Plugin\jtl_tmrapi\Middleware\APIAuthMiddleware;
use function Functional\first;

/**
 * Class Bootstrap
 * @package Plugin\jtl_tmrapi
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->hookInto(\HOOK_ROUTER_PRE_DISPATCH, function ($args) {
            $config = Shopsetting::getInstance()->getAll();
            // config array misused - yout could also set the plugin instance
            $config['link'] = $this->getPlugin()->getLinks()->getLinks()->first();
            /** @var Router $router */
            $router         = $args['router'];
            $controller     = new APIController(
                $this->getDB(),
                $this->getCache(),
                $router->getState(),
                $config,
                Shop::Container()->getAlertService()
            );
            $authMiddleware = new APIAuthMiddleware();
            //@see https://route.thephpleague.com/5.x/routes/
            $router->addRoute('tmrapi[/{id:\d+}]', [$controller, 'show']);
            $router->addRoute('tmrapi/{id:\d+}', [$controller, 'delete'], 'tmrapiDelete', ['DELETE'], $authMiddleware);
            $router->addRoute('tmrapi/{id:\d+}', [$controller, 'update'], 'tmrapiCreate', ['PUT']);
            $router->addRoute('tmrapi', [$controller, 'create'], 'tmrapiCreate', ['POST']);
            $router->addRoute('tmrapitest[/{slug}]', [$controller, 'getResponse'], 'tmaItemList');
        });
    }

    /**
     * @inheritdoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        if ($tabName === 'Item') {
            $controller = new BackendController(
                $this->getDB(),
                $this->getCache(),
                Shop::Container()->getAlertService(),
                Shop::Container()->getAdminAccount(),
                Shop::Container()->getGetText()
            );
            $controller->setAction($this->getPlugin()->getPaths()->getBackendURL());
            $controller->setRoute($this->getPlugin()->getPaths()->getBackendRoute());
            $response = $controller->getResponse(null, [], $smarty);
            if (\count($response->getHeader('location')) > 0) {
                \header('Location:' . first($response->getHeader('location')));
                exit();
            }

            return (string)$response->getBody();
        }

        return parent::renderAdminMenuTab($tabName, $menuID, $smarty);
    }
}
