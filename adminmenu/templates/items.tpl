{if $step === 'detail'}
    {include file='tpl_inc/model_detail.tpl' item=$item addChild=true}
{else}
    {include file='tpl_inc/model_list.tpl' items=$models delete=true create=true}
{/if}
