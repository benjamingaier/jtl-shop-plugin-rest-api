<?php declare(strict_types=1);

namespace Plugin\jtl_tmrapi\Controllers;

use JTL\Helpers\Request;
use JTL\Router\Controller\Backend\GenericModelController;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_tmrapi\Models\ApitestModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 *
 */
class BackendController extends GenericModelController
{
    private string $action;

    public function getResponse(?ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $this->smarty     = $smarty;
        $this->modelClass = ApitestModel::class;
        $step             = Request::getVar('action', 'overview');
        $smarty->assign('step', $step);

        return $this->handle(__DIR__ . '/../adminmenu/templates/items.tpl');
    }

    /**
     * @param string $action
     * @return void
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}
