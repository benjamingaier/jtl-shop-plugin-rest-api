<?php declare(strict_types=1);

namespace Plugin\jtl_tmrapi\Models;

use DateTime;
use Exception;
use JTL\Model\DataAttribute;
use JTL\Model\DataModel;
use JTL\Model\ModelHelper;
use JTL\Plugin\Admin\InputType;

/**
 *
 * @package Plugin\jtl_tmrapi\Models
 * @property int    $id
 * @method int getId()
 * @method void setId(int $value)
 * @property int    $languageID
 * @method int getLanguageID()
 * @method void setLanguageID(int $value)
 * @property int    $itemid
 * @method int getItemid()
 * @method void setItemid(int $value)
 * @property string $description
 * @method string getDescription()
 * @method void setDescription(string $value)
 */
final class ApitestLocalizationModel extends DataModel
{
    /**
     * @inheritdoc
     */
    public function getTableName(): string
    {
        return 'jtlapitestlocalization';
    }

    /**
     * Setting of keyname is not supported!
     * Call will always throw an Exception with code ERR_DATABASE!
     * @inheritdoc
     */
    public function setKeyName($keyName): void
    {
        throw new Exception(__METHOD__ . ': setting of keyname is not supported', self::ERR_DATABASE);
    }

    /**
     * @inheritdoc
     */
    public function getAttributes(): array
    {
        static $attributes = null;
        if ($attributes === null) {
            $attributes = [];
            $id         = DataAttribute::create('id', 'int', null, false, true);
            $id->getInputConfig()->setHidden(true);
            $attributes['id']         = $id;
            $lid                      = DataAttribute::create('languageID', 'int', null, false);
            $attributes['languageID'] = $lid;
            $itemid                   = DataAttribute::create('itemid', 'int', null, false);
            $itemid->getInputConfig()->setHidden(true);
            $attributes['itemid'] = $itemid;
            $text                 = DataAttribute::create('text', 'mediumtext', null, false, false);
            $text->getInputConfig()->setInputType(InputType::TEXTAREA);
            $attributes['text'] = $text;
        }

        return $attributes;
    }
}
