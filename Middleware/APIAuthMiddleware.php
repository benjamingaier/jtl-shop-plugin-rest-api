<?php declare(strict_types=1);

namespace Plugin\jtl_tmrapi\Middleware;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 *
 */
class APIAuthMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getHeaderLine('X_API_KEY') !== 'test123') {
            return new JsonResponse('No API key provided!', 403);
        }
        if ((int)\date('G') > 18) {
            return new JsonResponse('Too late', 403);
        }
        return $handler->handle($request);
    }
}
