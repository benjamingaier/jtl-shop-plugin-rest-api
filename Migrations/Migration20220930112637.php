<?php declare(strict_types=1);
/**
 * @package Plugin\jtl_tmrapi\Migrations
 * @author  Felix Moche
 */

namespace Plugin\jtl_tmrapi\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20220930112637
 * @package Plugin\jtl_tmrapi\Migrations
 */
class Migration20220930112637 extends Migration implements IMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `jtlapitest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `jtlapitest`');
    }
}
